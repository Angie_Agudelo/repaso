#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Simular cannon financiero
 				El usuario podrá ingresar a la opción Productos y servicios,
 				Luego podrá ingresar a la opción Leasing y finalmente diligenciar el formulario
 				de Leasing habitacional para verificar los valores arrojados.

  @CasoExitoso
  Scenario Outline: Diligenciar formulario
    Given Cargar datos
    | <Valor>  | <Plazo> | <Porcentaje>  | <Tasa> | 
    And Ingresar a la opción Productos y servicios
    And Ingresar a la opción Leasing
    And Ingresar a la opción Leasing Habitacional
    And Ingresar a la opción Simular cannon habitacional
    When Diligenciar formulario 
    Then Verificar valores
    And Mostrar valores por pantalla consola
    
    Examples: 
      | Valor  		| Plazo | Porcentaje  | Tasa | 
      | 26000000  | 24 		| 5  					| DTF  | 
    

  @CasoAlterno
  Scenario Outline: Simulación no exitosa
    Given Cargar datos
    | <Valor>  | <Plazo> | <Porcentaje>  | <Tasa> |  
    And Ingresar a la opción Productos y servicios
    And Ingresar a la opción Leasing
    And Ingresar a la opción Leasing Habitacional
    And Ingresar a la opción Simular cannon habitacional
    When Diligenciar formulario errado 
    Then Mensaje de alerta
    
    Examples: 
      | Valor  		| Plazo | Porcentaje  | Tasa | 
      | 4000000   | 24 		| 5  					| DTF  | 
