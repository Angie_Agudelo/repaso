package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.BuscarBancosPage;

public class BuscarBancosStep {
	
	BuscarBancosPage buscarBancosPage;
	
	public void IngresarInicio() {
		buscarBancosPage.open();
	}
	
	public void Buscar(List<List<String>> Data) throws Exception {	
		buscarBancosPage.DiligenciarBusqueda(Data.get(0).get(0).trim());
		buscarBancosPage.IconoBusqueda();
	}
	
	public void MostrarBancos(List<List<String>> Data) throws Exception {
		buscarBancosPage.AbrirPaginas(Data);
	}
	

}
