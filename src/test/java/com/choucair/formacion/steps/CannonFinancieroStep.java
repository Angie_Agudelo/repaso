package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.CannonFinancieroPage;

import net.thucydides.core.annotations.Step;

public class CannonFinancieroStep {

	CannonFinancieroPage cannonFinancieroPage;
	
	@Step
	public void CargarInformacion(List<List<String>> Data) {
		cannonFinancieroPage.SetValor(Data.get(0).get(0).trim());
		cannonFinancieroPage.SetPlazo(Data.get(0).get(1).trim());
		cannonFinancieroPage.SetPorcentaje(Data.get(0).get(2).trim());
		cannonFinancieroPage.SetTasa(Data.get(0).get(3).trim());
	}
	
	public void IngresarProductosServicios() {
		cannonFinancieroPage.open();
		cannonFinancieroPage.IngresarMenu();
	}
	
	public void IngresarLeasing() {
		cannonFinancieroPage.IngresarMenuPersonas();
	}
	
	public void IngresarLeasingHabitacional() {
		cannonFinancieroPage.IngresarLeasingHabitacional();
	}
	
	public void IngresarSimulador() {
		cannonFinancieroPage.IngresarSimulador();
	}
	
	public void Diligenciar() {
		cannonFinancieroPage.DiligenciarValor();
		cannonFinancieroPage.DiligenciarPlazo();
		cannonFinancieroPage.DiligenciarPorcentaje();
		cannonFinancieroPage.DiligenciarTasa();
		//cannonFinancieroPage.ValidarModalidad();
		cannonFinancieroPage.BotonSimular();
	}
	
	public void VerificarValores() {
		cannonFinancieroPage.ValidarTablaPorcentaje();
		cannonFinancieroPage.ValidarTablaValorCompra();
	}
	
	public void MostrarValores() {
		cannonFinancieroPage.MostrarTabla();
	}
	
	public void DiligenciarErrado() {
		cannonFinancieroPage.DiligenciarValor();
		cannonFinancieroPage.DiligenciarPlazo();
		cannonFinancieroPage.DiligenciarPorcentaje();
		cannonFinancieroPage.DiligenciarTasa();
	}
	
	public void VerificarMensaje() {
		cannonFinancieroPage.VerificarMensaje();
	}
}
