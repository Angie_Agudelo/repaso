package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.ComprarTVPageObject;

public class ComprarTVStep {

	ComprarTVPageObject comprarTVPageObject;
	
	public void IngresarPagina() {
		comprarTVPageObject.open();
		//comprarTVPageObject.CerrarInicio();
	}
	
	public void Buscar() {
		comprarTVPageObject.CampoBuscar();
		comprarTVPageObject.BotonBuscar();
	}
	
	public void SeleccionarParametros() throws InterruptedException {
		comprarTVPageObject.SeleccionarTama();		
		comprarTVPageObject.SeleccionarMarca();
		comprarTVPageObject.SeleccionarResolu();	
	}
	public void ImprimirConsola() {
		comprarTVPageObject.MostrarConsola();
	}
	public void SeleccionarTelevisor() {
		comprarTVPageObject.SeleccionarTelevisor();
	}
	
	public void AnadirProducto() {
		comprarTVPageObject.AnadirCarrito();
		comprarTVPageObject.IngresarDiv();
		comprarTVPageObject.CerrarPopup();
	}
	
	public void VerificaCarrito() {
		comprarTVPageObject.EntrarCarrito();
		comprarTVPageObject.VerificarCarrito();
	}

	
	
}
