package com.choucair.formacion.pageobjects;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.CloseWindow;
import org.openqa.selenium.remote.server.handler.FindElement;
import com.choucair.formacion.utilities.ExcelReader;
import com.choucair.formacion.utilities.Utilities;
import com.gargoylesoftware.htmlunit.javascript.host.Window;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("http://www.paginasamarillas.com.co/")
public class BuscarBancosPage extends PageObject{
	
	public String parentWindow;
	
	@FindBy (id="keyword")
	public WebElementFacade txtBusqueda;
	
	@FindBy (id="buscar")
	public WebElementFacade btnBusqueda;
	
	@FindBy (className ="col-sm-9 ")
	public WebElementFacade Contenido;
	
	@FindBy(xpath = "//div[@class='url']/a")
	public List<WebElement> strurls;
	
	@FindBy(xpath = "//")
	public WebElementFacade Titulo;
	
	public void DiligenciarBusqueda(String dato) {
		txtBusqueda.click();
		txtBusqueda.clear();
		txtBusqueda.sendKeys(dato);
	}
	
	public void IconoBusqueda() {
		btnBusqueda.click();
	}

	
	public void AbrirPaginas(List<List<String>> Data) throws Exception {			
		//parentWindow= getDriver().getWindowHandle();	
		ExcelReader.setExcelFile("D:\\Eclipse Oxygen\\com.choucair.base.repaso\\src\\test\\resources\\Datadriven\\dtDatos.xlsx", "Hoja1");
		for (int i = 0; i <10; i++) {
			System.out.println(strurls.get(i).getText());
			ExcelReader.setCellData(i+1, 1, strurls.get(i).getText());
				strurls.get(i).click();
				CambiarVentana(2, Data.get(0).get(i).trim());
			}
		ExcelReader.SaveData("D:\\Eclipse Oxygen\\com.choucair.base.repaso\\src\\test\\resources\\Datadriven\\dtDatos.xlsx");
		ExcelReader.CerrarBook();
		
	}
	
	public void CambiarVentana(int Ventana, String Dato) throws InterruptedException {
		Set<String> allWindows = getDriver().getWindowHandles();
		Integer intV = 0;
		for (String curVentana : allWindows) {	
			intV = intV + 1;
			if (intV == 1) parentWindow= getDriver().getWindowHandle();
			if (intV == Ventana) {
				getDriver().switchTo().window(curVentana);
				VerificarPagina(Dato);	
				getDriver().close();
				getDriver().switchTo().window(parentWindow);
				
				break;
			}
			esperar(3);
		}
	}
	
	public void esperar(int intEspera) throws InterruptedException {
		Thread.sleep(intEspera * 1000); 
	}
	
	
	public void VerificarPagina(String Dato) {
		WebElement Titulo =  getDriver().findElement(By.xpath("//html//head//title"));
		
		if(Titulo.getAttribute("text").contains(Dato)) {
			System.out.println("Verificación exitosa");
		}
	}
	

	
}
