package com.choucair.formacion.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


@DefaultUrl("https://www.exito.com/")
public class ComprarTVPageObject extends PageObject{

	
	@FindBy ( id = "tbxSearch")
	public WebElementFacade txtBuscar;
	
	@FindBy (id = "btnSearch")
	public WebElementFacade btnBuscar;
		
	@FindBy (id="close-wisepop-88598")
	public WebElementFacade Inicio;

	@FindBy (xpath="//*[@id=\'filterBy\']/div[2]/div[2]/ul/li[1]/label")
	public WebElementFacade Marcas;	
	
	@FindBy (xpath="//*[@id=\'filterBy\']/div[2]/div[2]/ul/li[2]/a")
	public WebElementFacade Tama;
	
	/*@FindBy (xpath="//li//a[contains(.,'UHD-4K')]") */
	public WebElement Resolucion;	

	
	@FindBy (xpath = "//*[@id=\'plp\']/div[4]/div[2]/div/div[2]/div[1]/p")
	public WebElementFacade Resulados;
	
	@FindBy (className="product-list")
	public WebElementFacade Lista;
	
	@FindBy (className="btn-add-cart")
	public WebElementFacade Anadir;
		
	@FindBy (className="close_bunting_lightbox_X")
	public WebElementFacade Cerrar;
	
	@FindBy (className="icoe-carro-compras-lleno")
	public WebElementFacade Carrito;
	
	@FindBy (tagName = "h4")
	public WebElementFacade LCarrito;
	
	@FindBy (id = "bunting_lightbox-710")
	public WebElementFacade Popup;
	
	public void CampoBuscar() {
		txtBuscar.click();
		txtBuscar.clear();
		txtBuscar.sendKeys("Televisor");
	}
	
	
	public void BotonBuscar() {
		btnBuscar.click();
	}
	
	public void CerrarInicio() {
		Inicio.click();
	}
	
	public void SeleccionarMarca() throws InterruptedException {
		Marcas.click();
		
	}
	public void SeleccionarTama() throws InterruptedException {
		Tama.click();
		esperar(5);
	}
	
	public void SeleccionarResolu()   {
		Resolucion  = getDriver().findElement(By.xpath("//li/a[contains(.,'UHD-4K')]"));
		Resolucion.click();		
	}

	
	public void esperar(int intEspera) throws InterruptedException {
		Thread.sleep(intEspera * 1000); 
	}
	
	
	public void MostrarResultados() {
		String LabelR = Resulados.getText();
		System.out.println(LabelR);
	}
	
	public void MostrarConsola() {
		List<WebElement> Filas = Lista.findElements(By.className("col-data"));
		for (int i=0; i<Filas.size();i++) {
			System.out.println(Filas.get(i).getText());
			System.out.println("_____________________________________________________________");
		}
	}
	public void SeleccionarTelevisor() {
		List<WebElement> Filas = Lista.findElements(By.className("col-data"));
		for (int i=0; i<Filas.size();i++) {
			if (i==0) {
				Filas.get(i).click();
			}
		}
	}
	
	public void AnadirCarrito(){
		Anadir.click();	
	}
	public void CerrarPopup(){
		Cerrar.click();
	}
	
	
	public void EntrarCarrito(){
		
		boolean clicked = false;
		do {
			try {
				Carrito.click();
			}
			catch (WebDriverException e) {
				continue;
			} finally {
				clicked = true;
			}
		}
		while (!clicked);
		
	}
	
	public void IngresarDiv() {
		getDriver().switchTo().equals(Popup);
	}
	
	public void VerificarCarrito(){
		String Label= "1 productos en tu carrito";
		String Mensaje = LCarrito.getText();
		assertThat(Mensaje, containsString(Label));
	}
}
