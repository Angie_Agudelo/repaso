package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class CannonFinancieroPage extends PageObject {
	
	//Métodos get y set
	private String Valor;
	private String Plazo;
	private String Porcentaje;
	private String Tasa;
	
	public void SetValor(String valor) {
		this.Valor=valor;
	}
	
	public String GetValor() {
		return Valor;
	}
	
	public void SetPlazo (String plazo) {
		this.Plazo=plazo;
	}
	
	public String GetPlazo() {
		return Plazo;
	}
	
	public void SetPorcentaje (String porcentaje) {
		this.Porcentaje = porcentaje;
	}
	
	public String GetPorcentaje() {
		return Porcentaje;
	}
	public void SetTasa (String tasa) {
		this.Tasa = tasa;
	}
	
	public String GetTasa () {
		return Tasa;
	}
	
	
	//Captura de campos
	@FindBy (id = "main-menu")
	public WebElementFacade MenuPpal;
	
	@FindBy (id = "productosPersonas")
	public WebElementFacade MenuPersonas;
	
	@FindBy (className = "col-xs-12")
	public WebElementFacade Leasing;
	
	@FindBy ( xpath = "//a[@title= \'Leasing Habitacional\']")
	public WebElementFacade LeasingHabitacional;
	
	@FindBy ( xpath = "//a[@title= \'Simular Canon Constante\']")
	public WebElementFacade btnSimulador;
	
	@FindBy ( name="valorActivo")
	public WebElementFacade txtValor;
	
	@FindBy ( name="plazo")
	public WebElementFacade txtPlazo;
	
	@FindBy ( name="opcionCompra")
	public WebElementFacade txtPorcentaje;
	
	@FindBy ( name="comboTipoTasa")
	public WebElementFacade cmbTasa;
	
	@FindBy ( xpath = "//input[@value='Vencida']")
	public WebElementFacade txtModalidad;
	
	@FindBy ( className = "table-datos")
	public WebElementFacade tblTabla;
	
	@FindBy ( name = "simular")
	public WebElementFacade btnSimular;
	
	@FindBy ( xpath = "//span[@class=\'ng-binding\']")
	public WebElementFacade MensajeError;
	
	
	public void IngresarMenu() {
		List<WebElement> Filas = MenuPpal.findElements(By.className("toggle-submenu"));
		
		for (int i=0; i<Filas.size();i++) {
			if (Filas.get(i).getText().equals("Productos y Servicios")) {
				Filas.get(i).click();
			}
		}
	}
	public void IngresarMenuPersonas() {
		List<WebElement> Filas = MenuPersonas.findElements(By.className("col-xs-4"));
		
		for (int i=0; i<Filas.size();i++) {
			if (Filas.get(i).getText().equals("Leasing")) {
				Filas.get(i).click();
				i=Filas.size();
			}
		}
	}
	
	public void IngresarLeasingHabitacional() {
		LeasingHabitacional.click();
	}
	
	public void IngresarSimulador() {
		btnSimulador.click();
	}
	
	
	public void DiligenciarValor() {
		txtValor.click();
		txtValor.clear();
		txtValor.sendKeys(GetValor());
	}
	
	public void DiligenciarPlazo() {
		txtPlazo.click();
		txtPlazo.clear();
		txtPlazo.sendKeys(GetPlazo());
	}
	
	public void DiligenciarPorcentaje() {
		txtPorcentaje.click();
		txtPorcentaje.clear();
		txtPorcentaje.sendKeys(GetPorcentaje());
	}
	
	public void DiligenciarTasa() {
		cmbTasa.click();
		cmbTasa.selectByVisibleText(GetTasa());
	}
	
	public void ValidarModalidad() {
		String Campo = txtModalidad.getText();
		String Mensaje = "Vencida";
		assertThat(Campo, containsString(Mensaje));
	}
	
	public void BotonSimular() {
		btnSimular.click();
	}
	
	
	
	public void ValidarTablaValorCompra() {
		List<WebElement> Filas = tblTabla.findElements(By.className("ng-binding"));
		String LabelV = "";
		for (int i=0; i<Filas.size();i++) {
			if (i == 7) {
				String Valor = Filas.get(i).getText();
				LabelV = Valor.replaceAll("\\$", "").replaceAll("\\,", "").replaceAll("\\.00", "");
			}
		}
		int Val = Integer.parseInt(GetValor()) * ((Integer.parseInt(GetPorcentaje())/100));
		String V = String.valueOf(Val);
		assertThat(LabelV, containsString(V));
		
	}
	public void ValidarTablaPorcentaje() {
		List<WebElement> Filas = tblTabla.findElements(By.className("ng-binding"));
		String LabelPor = "";
		for (int i=0; i<Filas.size();i++) {
			if (i == 9) {
				String Porcentaje = Filas.get(i).getText();
				LabelPor = Porcentaje.replaceAll("\\$", "").replaceAll("\\,", "").replaceAll("\\.00", "");
			}
						
		}
		assertThat(LabelPor, containsString(GetPorcentaje()));
	}
	
	public void MostrarTabla() {

		List<WebElement> Filas = tblTabla.findElements(By.className("ng-binding"));
		
		for (int i=0; i<Filas.size();i++) {
			System.out.println(Filas.get(i).getText());
		}
		
	}
	
	public void VerificarMensaje() {

		String LabelM = MensajeError.getText();
		String Mensaje = "El campo no cumple con el valor mínimo $ 10,000,000";
		assertThat(Mensaje, containsString(LabelM));
		
	}
	
}
