package com.choucair.formacion.utilities;

public class Utilities {

	private static final String SEPARADOR = "/";

	private static final String RUTARECURSOS = SEPARADOR + "src" + SEPARADOR + "test" + SEPARADOR + "resources";

	private static String rutaAbsoluta() {
		String ruta = System.getProperty("user.dir");
		ruta += RUTARECURSOS;
		return ruta;

	}

	public static String obtenerRutaExel(String archivo) {

		return rutaAbsoluta() + SEPARADOR + "Datadriven" + SEPARADOR + archivo;
	}

}
