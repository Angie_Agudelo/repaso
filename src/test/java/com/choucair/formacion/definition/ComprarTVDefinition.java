package com.choucair.formacion.definition;

import com.choucair.formacion.steps.ComprarTVStep;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ComprarTVDefinition {

	@Steps
	ComprarTVStep comprarTVStep;
	
	@Given("^Ingresar a la página del Éxito$")
	public void ingresar_a_la_página_del_Éxito() throws Throwable {
	    comprarTVStep.IngresarPagina();
	}

	@Given("^Buscar televisores$")
	public void buscar_televisores() throws Throwable {
	   comprarTVStep.Buscar();
	}

	@Given("^Seleccionar parámetros de búsqueda$")
	public void seleccionar_parámetros_de_búsqueda() throws Throwable {
	    comprarTVStep.SeleccionarParametros();
	}

	@When("^Imprimir en consola la descripción de los televisores$")
	public void imprimir_en_consola_la_descripción_de_los_televisores() throws Throwable {
	    comprarTVStep.ImprimirConsola();
	}

	@When("^Seleccionar televisor$")
	public void seleccionar_televisor() throws Throwable {
	    comprarTVStep.SeleccionarTelevisor();
	}

	@When("^Añadir producto al carrito$")
	public void añadir_producto_al_carrito() throws Throwable {
	    comprarTVStep.AnadirProducto();
	}

	@Then("^Verificar producto en el carrito$")
	public void verificar_producto_en_el_carrito() throws Throwable {
	    comprarTVStep.VerificaCarrito();
	}
	
}
