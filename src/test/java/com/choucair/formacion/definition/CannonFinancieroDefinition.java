package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.CannonFinancieroStep;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import net.thucydides.core.annotations.Steps;


public class CannonFinancieroDefinition {

	@Steps
	CannonFinancieroStep cannonFinancieroStep;
			
	@Given("^Cargar datos$")
	public void cargar_datos(DataTable Datos) throws Throwable {
	   List<List<String>> Data = Datos.raw();
	   cannonFinancieroStep.CargarInformacion(Data);
	}

	@Given("^Ingresar a la opción Productos y servicios$")
	public void ingresar_a_la_opción_Productos_y_servicios() throws Throwable {
		cannonFinancieroStep.IngresarProductosServicios();
	}

	@Given("^Ingresar a la opción Leasing$")
	public void ingresar_a_la_opción_Leasing() throws Throwable {
	 cannonFinancieroStep.IngresarLeasing();
	}

	@Given("^Ingresar a la opción Leasing Habitacional$")
	public void ingresar_a_la_opción_Leasing_Habitacional() throws Throwable {
	 cannonFinancieroStep.IngresarLeasingHabitacional();
	}

	@Given("^Ingresar a la opción Simular cannon habitacional$")
	public void ingresar_a_la_opción_Simular_cannon_habitacional() throws Throwable {
	  cannonFinancieroStep.IngresarSimulador();
	}

	@When("^Diligenciar formulario$")
	public void diligenciar_formulario() throws Throwable {
	    cannonFinancieroStep.Diligenciar();
	}
	
	@When("^Diligenciar formulario errado$")
	public void diligenciar_formulario_errado() throws Throwable {
	    cannonFinancieroStep.DiligenciarErrado();
	}

	@Then("^Verificar valores$")
	public void verificar_valores() throws Throwable {
	    cannonFinancieroStep.VerificarValores();
	}

	@Then("^Mostrar valores por pantalla consola$")
	public void mostrar_valores_por_pantalla_consola() throws Throwable {
	    cannonFinancieroStep.MostrarValores();
	}

	@Then("^Mensaje de alerta$")
	public void mensaje_de_alerta() throws Throwable {
	    cannonFinancieroStep.VerificarMensaje();
	}
	
	
}
