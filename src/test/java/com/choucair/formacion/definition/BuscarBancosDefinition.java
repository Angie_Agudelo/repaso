package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.BuscarBancosStep;


import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class BuscarBancosDefinition {
	
	@Steps
	BuscarBancosStep buscarBancosStep;
	
	
	@Given("^Ingresar a la url de las páginas amarillas$")
	public void ingresar_a_la_url_de_las_páginas_amarillas() throws Throwable {
	    buscarBancosStep.IngresarInicio();
	}

	@Given("^Buscar los bancos$")
	public void buscar_los_bancos(DataTable Datos) throws Throwable {
		 List<List<String>> Data = Datos.raw();
		buscarBancosStep.Buscar(Data);
	}

	@When("^Ingresar a la página y verificarla$")
	public void ingresar_a_la_página(DataTable Datos) throws Throwable {
		List<List<String>> Data = Datos.raw();
	    buscarBancosStep.MostrarBancos(Data);
	    //ExcelReader.setExcelFile("D:\\Eclipse Oxygen\\com.choucair.base.repaso\\src\\test\\resources\\Datadriven\\Bancos.xlsx", "Hoja1");
	}

	@Then("^Agrgear URL al archivo de Excel$")
	public void agrgear_URL_al_archivo_de_Excel() throws Throwable {
		
	}

}
