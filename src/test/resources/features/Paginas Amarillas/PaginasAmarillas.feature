#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Buscar bancos páginas amarillas
  Ingresar a la url de las página amarillas y buscar los bancos almacenando los primeros 10.


  @CasoExitoso
  Scenario Outline: Buscar bancos
    Given Ingresar a la url de las páginas amarillas
    And Buscar los bancos 
    		|<Busqueda>|
    When Ingresar a la página y verificarla
    | <Banco uno> | <Banco dos> | <Banco tres>  | <Banco cuatro> | <Banco cinco> | <Banco seis> | <Banco siete> | <Banco ocho> | <Banco nueve> | <Banco diez> |
    Then Agrgear URL al archivo de Excel

    Examples: 
      | Busqueda  | Banco uno | Banco dos | Banco tres  | Banco cuatro | Banco cinco | Banco seis | Banco siete | Banco ocho | Banco nueve | Banco diez |
      | Bancos	  | COBRANZAS | Portal 80 | Camino Real | Banco W      | Comultrasan | Personas   | DOGMAN 			| GER				 | Host        | Palmetto   |
